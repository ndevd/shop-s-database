CREATE DATABASE tienda;
USE tienda;

CREATE TABLE producto(
	id_articulo INT AUTO_INCREMENT, 
	nombre VARCHAR(30), 
	rubro VARCHAR(20), 
	descripcion VARCHAR(60), 
	precio_unitario FLOAT, 
	id_proveedor INT, 
	PRIMARY KEY(id_articulo)
);